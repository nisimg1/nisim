﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnDieEvent : MonoBehaviour
{
   // [SerializeField] UnityEvent _onDieEvent;
    [SerializeField] Objective[] requerdToMoveOn;
    void Start()
    {
        GetComponent<Health>().OnDie += OnDieEvent_OnDie;
    }

    private void OnDieEvent_OnDie()
    {
        foreach (var item in requerdToMoveOn)
        {
            if (ObjectiveManager._listOfQuest.Contains(item) && !item.MissionAccomplished)
            {
                if (item is ObjectiveInt objectiveInt)
                {
                    objectiveInt.Modify();
                }
            }
        }
           
        
    }

   
}
